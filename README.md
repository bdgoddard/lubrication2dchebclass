# README #

2DChebClass is a code Andreas Nold and Ben Goddard developed as a project for DFT and DDFT computations in 2012 at Imperial College London. Since then, it evolved into a library of classes and functions to solve 1D and 2D DFT and DDFT problems.  This repository contains additional code by Ben Goddard and Rory Mills-Williams to implement lubrication forced in inertial dynamics; see .

The directory Computations/POF_HI_Lubrication/ which contains the necessary files to make Figures 2,3,4,6,8,9b,10,11,12 as found in the paper The Singular Hydrodynamic Interactions Between Two Spheres In Stokes Flow. B. D. Goddard, R. D. Mills-Williams and J. Sun. POF20-AR-00857R.

Files/ contains the MATLAB scripts for the figures.

Data/ contains xyz files used to create Figure 9a.

Figures 2,3,4,6,8,9b are produced by running the corresponding MATLAB scripts.

Figures 10,11,12 are are produced by running compareDDFTs with the appropriate figure name as the input file.

Note that you will need to run AddPaths before running these computations.  It is also advisable to change dirData to your preferred data directory.