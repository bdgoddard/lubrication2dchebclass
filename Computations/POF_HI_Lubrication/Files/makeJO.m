function K=XmakeJO(x,sigmaH,dim)
%   returns Z1 part of the grand resistance matrix at positions for particles at
%   positions x.  See Jeffrey & Onishi (1984)
%
% INPUTS:
%   x           -- (dim*nParticles,1) vector of positions
%   sigmaH      -- hydrodynamic diameter
%   dim         -- dimension
%
% OUTPUTS:
%   w2          -- (dim*nParticles,dim*nParticles) tensor

% get \hhat r_ij \otimes \hat r_ij and r_ij^{-1}
[RoR,~]=makeRoR(x,dim);
[Rij,~]=getRij(x,x,dim);     % nParticles x nParticles

hij = Rij - sigmaH*(Rij~=0);

% number of particles
nParticles=length(x)/dim;

% mask to copy row/columnwise nParticles times
dvec=(1:dim)';                        % column vector of 1:dim
dmask=dvec(:,ones(nParticles,1));     % nParticles columns each 1:dim
% so dmask(:) is 1:dim nParticles times

% identity matrix in each dim x dim block, except those on the diagonal
eyes=eye(dim);      % matrix for the dim x dim block
eyes=eyes(dmask,dmask) - eye(dim*nParticles);

% calculate w1
%w2=3/8*sigmaH*RijInv .*(eyes + RoR) + 1/16*sigmaH^3*RijInv.^3.*(eyes-3*RoR);
% w2=3/8*diag(sigmaH)*RijInv .*(eyes + RoR);

normalforce = zeros(size(hij));

for i = 1:nParticles
    for j = 1:nParticles
        if hij(i,j) == 0
            normalforce(i,j) = 0;
        else 
            centreDistance = hij(i,j) + sigmaH;
            normalforce(i,j) = -XA11e(sigmaH,centreDistance);
            normalforce(i,j) = normalforce(i,j) + 1;
        end
    end
end

tangentialforce = zeros(size(hij));

for i = 1:nParticles
    for j = 1:nParticles
        if hij(i,j) == 0
            tangentialforce(i,j) = 0;
        else
            centreDistance = hij(i,j) + sigmaH;   
            tangentialforce(i,j) = -YA11e(sigmaH,centreDistance);
            tangentialforce(i,j) = tangentialforce(i,j) + 1;
        end
    end
end


% normalforcerep = kron(normalforce,ones(3,3));
% tangentialforcerep =  kron(tangentialforce,ones(3,3));



normalforcerep = repelem(normalforce,dim,dim).*RoR;
tangentialforcerep = repelem(tangentialforce,dim,dim).*(eyes-RoR);

K = normalforcerep + tangentialforcerep;
KDiag = zeros(size(K));
for i = 1:dim:nParticles*dim
    rowSum = 0;
    for j = 1:dim:nParticles*dim
        rowSum = K(i:i+dim-1,j:j+dim-1) + rowSum;
    end
    KDiag(i:i+dim-1,i:i+dim-1) = -rowSum;
end

K = K + KDiag;
I=eye(dim*nParticles); 

K = (K + I);


% K = -[Z1,-Z1;-Z1,Z1];

% w1=hij; % element wise multiplication
% w1=normalforcerep.*RoR + (eyes-RoR).*tangentialforcerep;
% Z1 = [];
% Z1=normalforcerep.*RoR + (eyes-RoR).*tangentialforcerep;

% w1 = [Z1,-Z1;-Z1,Z1];
end


% OLD VERSION -- slow but understandable
% % number of colloid particles
% N=length(x)/dim;
%
% % define a mask whose columns are the positions of the 3 coordinates of each
% % colloid particle in r
% mask=reshape(1:dim*N,dim,N);
%
% w2=zeros(dim*N,dim*N);
% % Z2 is a function of |r_i-r_j| and hence is symmetric.  Also, Z2==0 on
% % diagonal blocks. Only calculate half of it, then add the transpose.
% for iRow=1:N
%     for iCol=iRow+1:N;
%         % r_i-r_j
%         R=x(mask(:,iRow))-x(mask(:,iCol));
%
%         % r_{ij} \otimes r_{ij} / || r_{ij} ||^2
%         normR=norm(R);
%         RoR=R*R'/normR^2;
%
%         w2(mask(:,iRow),mask(:,iCol)) = ( 3/8*sigmaH/normR * ( eye(dim) + RoR )  ...
%             +1/16*(sigmaH/normR)^3 * (eye(dim) - 3*RoR) );
%     end
% end
% % % add the transpose to complete the calculation -- note there are no
% % % diagonal elements so we don't need to worry about double counting
% w2=w2+w2';

