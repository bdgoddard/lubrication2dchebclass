% Figure 4 %

close all force
clear all

set(0,'defaultaxesfontsize',20);
set(0,'defaulttextfontsize',20);
set(0,'defaultaxeslinewidth',1.5);
set(0,'defaultlinelinewidth',1.5);

ymin = 1e-3; N = 101; geometricL = 1;
hpts = genForcePts(ymin,N,geometricL);
r1 = 1/2; r2 = 1/2;
dpts = hpts + r1 + r2;

tol = 1e-12;
    
myf = gms(r1,r2,hpts);
fkk = XA11e(r1,r2,hpts);
beta = 2*r2;
f1f2 = zeros(2,N);
for i=1:N
    [s1,s2] = makeNormalScalar(dpts(i),beta,tol);
    f1f2(:,i)=[s1;s2];
end

jo = zeros(N,2); 

for i=1:N;
    jo(i,1) = hpts(i); 
    jo(i,2) = XA11(r1,r2,hpts(i));
end
%
figure

l1 = loglog(hpts,-f1f2(1,:), 'k-');hold on
l2 = loglog(hpts, myf, 'k-.');  
l3 = loglog(hpts, fkk, 'b-.');
l4 = loglog(hpts, jo(:,2),'r-.');
xlim([1e-3 1e2]), ylim([1e-2 1e3])
hold off
grid on
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
l=legend([l1 l2 l3 l4], '$F^1_{z}$','$F_z^e$','$X^A_{11}$ equation (3.17)', '$X^A_{11}$ equation (3.13)');
ylabel('$F/ 6 \pi\mu r_1 U$','interpreter','latex','fontsize',22);
xlabel('$h/\sigma$','interpreter','latex','fontsize',22);
set(l, 'interpreter','latex','fontsize',24, 'location','best');
title({'Force'},'interpreter', 'latex', 'fontsize',24);
shg
%
clear all

ymin = 1e-3; N = 101; geometricL = 1;
hpts = genForcePts(1e-3,N,geometricL);
r1 = 1/2; r2 = 2*pi/2;
dpts = hpts + r1 + r2;
beta = 2*r2;
tol = 1e-12;
    
myf = gms(r1,r2,hpts);
fkk = XA11e(r1,r2,hpts);


f1f2 = zeros(2,N);
for i=1:N
    [s1,s2] = makeNormalScalar(dpts(i),beta,tol);
    f1f2(:,i)=[s1;s2];
end

jo = zeros(N,2); 

for i=1:N;
    jo(i,1) = hpts(i); 
    jo(i,2) = XA11(r1,r2,hpts(i));
end

figure

l1 = loglog(hpts,-f1f2(1,:), 'k-');hold on
l2 = loglog(hpts, myf, 'k-.');  
l3 = loglog(hpts, fkk, 'b-.');
l4 = loglog(hpts, jo(:,2),'r-.');
xlim([1e-3 1e2]), ylim([1e-2 1e3])
hold off
grid on
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
l=legend([l1 l2 l3 l4], '$F^1_{z}$','$F_z^e$','$X^A_{11}$ equation (3.17)', '$X^A_{11}$ equation (3.13)');
ylabel('$F/ 6 \pi\mu r_1 U$','interpreter','latex','fontsize',22);
xlabel('$h/\sigma$','interpreter','latex','fontsize',22);
set(l, 'interpreter','latex','fontsize',24, 'location','best');
title({'Force'},'interpreter', 'latex', 'fontsize',24);
shg
