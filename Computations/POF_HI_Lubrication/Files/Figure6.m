% Figure 6a

close all force
clear all

fs = 24;

set(0,'defaultaxesfontsize',20);
set(0,'defaulttextfontsize',20);
set(0,'defaultaxeslinewidth',1.5);
set(0,'defaultlinelinewidth',1.5);

lambda = 1; sigmaH = 1;
r1 = 1/2; r2 = lambda/2;

geom.N = 200; geom.yMin = 1e-3; geom.yMax = 70; geom.L = .5;
aline = SpectralLine(geom);
aline.ComputeAll();
evalpts = aline.Pts.y + r1 + r2 ;
dMin = geom.yMin + r1 + r2;

% NORMAL COMPONENT XA11
toleta = 1e-12;

xA11jo = [];
for i = 1:length(evalpts)
   xA11jo = [xA11jo; XA11s(lambda,evalpts(i))]; 
end

x11gms = []; 

for i = 1:length(evalpts)
   [temp,~] = makeNormalScalar(evalpts(i),sigmaH,toleta);
   x11gms = [x11gms; temp]; 
end

% TANGENTIAL COMPONENT YA11 
toleta = 1e-12;

yA11jo = [];
for i = 1:length(evalpts)
   yA11jo = [yA11jo; YA11(lambda,evalpts(i))]; 
end

y11gms = []; 

for i = 1:length(evalpts)
   [temp,~] = makeTangentScalar(evalpts(i),sigmaH,toleta);
   y11gms = [y11gms; temp]; 
end

%
fig1 = figure(1);
p1 = loglog(aline.Pts.y,-x11gms,'k-'); hold on;
p2 = loglog(aline.Pts.y,xA11jo,'r-');

p3 = loglog(aline.Pts.y,-y11gms,'k-.');
p4 = loglog(aline.Pts.y,yA11jo,'r-.');

grid on
ylabel('$F$','interpreter', 'latex', 'fontsize', fs)

ylim([1e-2 1e3]);
xlim([7e-4 1e2]);

hold off

title('Nondimensional Force','interpreter', 'latex', 'fontsize', fs)
xlabel('$h/\sigma$','interpreter', 'latex', 'fontsize', fs-4)
ylabel('F','interpreter', 'latex', 'fontsize', fs-4)

l1=legend([p1 p2 p3 p4], '$\mathcal{F}_z$ GMS','$X^A_{11}$ Jeffrey \& Onishi (1984)','Relative error', 'Relative error');
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
set(l1, 'interpreter','latex','fontsize',fs, 'location','best');

fig2 = figure(2);
p5 = loglog(aline.Pts.y, abs(x11gms+xA11jo)./abs(x11gms),'-','color',[1 .5 0]); hold on
p6 = loglog(aline.Pts.y, abs(y11gms+yA11jo)./abs(y11gms),'-.','color',[1 .5 0]); 

grid on

ylim([1e-2 2e0]);
xlim([1e-3 1e2]);

title('Error','interpreter', 'latex', 'fontsize', fs)
ylabel('Relative Error','interpreter', 'latex', 'fontsize', fs-4)

hold off

[h_m h_i]=inset(fig1,fig2);

l2=legend([p5 p6],'Relative error', 'Relative error');
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
set(l2, 'interpreter','latex','fontsize',fs, 'location','best');

shg

% Figure 6b %
clear all

fs = 24;

set(0,'defaultaxesfontsize',20);
set(0,'defaulttextfontsize',20);
set(0,'defaultaxeslinewidth',1.5);
set(0,'defaultlinelinewidth',1.5);

lambda = 1; sigmaH = 1;
r1 = 1/2; r2 = lambda/2;

geom.N = 200; geom.yMin = 1e-2; geom.yMax = 70; geom.L = .5;
aline = SpectralLine(geom);
aline.ComputeAll();
evalpts = aline.Pts.y + r1 + r2 ;
dMin = geom.yMin + r1 + r2;

% NORMAL COMPONENT XA11e
toleta = 1e-9;

xA11jo = [];
for i = 1:length(evalpts)
   xA11jo = [xA11jo; XA11e(lambda,evalpts(i))]; 
end

x11gms = []; 

for i = 1:length(evalpts)
   [temp,~] = makeNormalScalar(evalpts(i),sigmaH,toleta);
   x11gms = [x11gms; temp]; 
end

% TANGENTIAL COMPONENT YA11e
toleta = 1e-12;

yA11jo = [];
for i = 1:length(evalpts)
   yA11jo = [yA11jo; YA11e(lambda,evalpts(i))]; 
end

y11gms = []; 

for i = 1:length(evalpts)
   [temp,~] = makeTangentScalar(evalpts(i),sigmaH,toleta);
   y11gms = [y11gms; temp]; 
end

%
fig1 = figure(1);
p1 = loglog(aline.Pts.y,-x11gms,'k-'); hold on;
p2 = loglog(aline.Pts.y,xA11jo,'b-');

p3 = loglog(aline.Pts.y,-y11gms,'k-.');
p4 = loglog(aline.Pts.y,yA11jo,'b-.');

grid on

ylabel('$F$','interpreter', 'latex', 'fontsize', fs)

ylim([1e-2 1e2]);
xlim([8e-3 1e2]);

hold off

title('Nondimensional Force','interpreter', 'latex', 'fontsize', fs)
xlabel('$h/\sigma$','interpreter', 'latex', 'fontsize', fs-4)
ylabel('F','interpreter', 'latex', 'fontsize', fs-4)

l1=legend([p1 p2 p3 p4], '$\mathcal{F}_z$ GMS','$X^A_{11}$ Jeffrey \& Onishi (1984)','Relative error', 'Relative error');
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
set(l1, 'interpreter','latex','fontsize',fs, 'location','best');

fig2 = figure(2);
p5 = loglog(aline.Pts.y, abs(x11gms+xA11jo)./abs(x11gms),'-', 'color',[1 .5 0]); hold on
p6 = loglog(aline.Pts.y, abs(y11gms+yA11jo)./abs(y11gms),'-.','color',[1 .5 0]); 
grid on

ylim([1e-2 5e0]);
xlim([7e-3 1e2]);

hold off

title('Error','interpreter', 'latex', 'fontsize', fs)
ylabel('Relative Error','interpreter', 'latex', 'fontsize', fs-4)


[h_m h_i]=inset(fig1,fig2);

l2=legend([p5 p6],'Relative error', 'Relative error');
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
set(l2, 'interpreter','latex','fontsize',fs, 'location','best');

shg