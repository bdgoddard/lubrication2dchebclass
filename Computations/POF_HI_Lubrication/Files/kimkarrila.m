function fkk = kimkarrila(beta,h)
    fkk = (beta^2/(1+beta)^2./(h)+1/5*beta*(1+7*beta+beta^2)/(1+beta)^3*log(1./((h))));
end