% COMPUTEEIGSOFR computes the smallest eigenvalue of R for a range of
% volume fractions for a system of spheres in regular packing
% configuration.

% INPUTS 0 < volFracMax < 1 - a percentage of the efficient sphere packing pi/(3*sqrt(2))
%        0 < volFracMin < 1 - a percentage of the efficient sphere packing pi/(3*sqrt(2))
%        sigmaH - hydrodynamic diameter
%        dim - dimension
%        tol - tolerance for a picard iteration

function [mineigs,volFracPts] = computeEigsOfR(volFracMax,volFracMin,numberOfVolumeFracs,sigmaH,dim,tol,varargin)

sigmaTemp = sigmaH;

geom.N = numberOfVolumeFracs; geom.yMin = volFracMin; geom.yMax = volFracMax;
geom.L = .5;

aline = SpectralLine(geom);
aline.ComputeAll();
volFracPts = aline.Pts.y;

minEigOfKGMS = [];
minEigOfKJO = [];
minEigOfKJOarb = [];

disp('Computing eigenvalues of R as a function of volume fraction for spherical bipolar, Kim & Karrila, and Jeffrey & Onishi...')

for i = 1:numberOfVolumeFracs
    centredistanceTemp = sigmaTemp./volFracPts(i);
    [centresTemp, ~]= makeRegularPacking(sigmaH,centredistanceTemp,dim,1);
    
    KGMS = makeGMS(centresTemp,sigmaH,dim,tol);
    KJO=makeJO(centresTemp,sigmaH,dim);
    KJOarb=makeJOarb(centresTemp,sigmaH,dim);
    
    minEigKGMS = min(eig(KGMS));
    minEigKJO = min(eig(KJO));
    minEigKJOarb = min(eig(KJOarb));
    
    minEigOfKGMS = [minEigOfKGMS; minEigKGMS];
    minEigOfKJO = [minEigOfKJO; minEigKJO];
    minEigOfKJOarb = [minEigOfKJOarb; minEigKJOarb];
end


mineigs = [minEigOfKGMS,minEigOfKJO,minEigOfKJOarb];

if strcmp(varargin,'plot');
    disp('Plotting smallest eigenvalue for each three...')
    
    set(0,'defaultaxesfontsize',20);
    set(0,'defaulttextfontsize',20);
    set(0,'defaultaxeslinewidth',1.5);
    set(0,'defaultlinelinewidth',1.5);
    
    figure;
    plot(volFracPts,minEigOfKGMS,'k-'); hold on
    plot(volFracPts,minEigOfKJO,'b-');
    plot(volFracPts,minEigOfKJOarb,'r-'); hold off
    shg
end

end