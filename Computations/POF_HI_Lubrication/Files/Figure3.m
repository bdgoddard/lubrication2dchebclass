% Figure 3 %

close all force
clear all

set(0,'defaultaxesfontsize',20);
set(0,'defaulttextfontsize',20);
set(0,'defaultaxeslinewidth',1.5);
set(0,'defaultlinelinewidth',1.5);

fs = 24;
N = 200;
pts = genForcePts(1e-3,N,1);
toleta = 1e-12;

% a %
% beta = 1.2 %

r1 = 1/2; r2 = 1.2/2;
evalpts = pts + r1 + r2;

beta = 2*r2;
F1F2 = zeros(2,N);

for i=1:N
    [FSphere1, FSphere2] = makeNormalScalar(evalpts(i),beta,toleta);
    F1F2(:,i)=[FSphere1;FSphere2];
end

onePointTwo=F1F2;

%  beta = 2  %

r1 = 1/2; r2 = 2/2;
evalpts = pts + r1 + r2 ;

beta = 2*r2;
F1F2 = zeros(2,N);
for i=1:N
    [FSphere1, FSphere2] = makeNormalScalar(evalpts(i),beta,toleta);
    F1F2(:,i)=[FSphere1;FSphere2];
end
two=F1F2;

% beta = 5 %

r1 = 1/2; r2 = 5/2;
evalpts = pts + r1 + r2 ;

beta = 2*r2;
F1F2 = zeros(2,N);
for i=1:N
    [FSphere1, FSphere2] = makeNormalScalar(evalpts(i),beta,toleta);
    F1F2(:,i)=[FSphere1;FSphere2];
end
five=F1F2;
 
% beta = 12 %

r1 = 1/2; r2 = 12/2;
evalpts = pts + r1 + r2 ;

beta = 2*r2;
F1F2 = zeros(2,N);
for i=1:N
    [FSphere1, FSphere2] = makeNormalScalar(evalpts(i),beta,toleta);
    F1F2(:,i)=[FSphere1;FSphere2];
end
twelve=F1F2;

figure
p1 = loglog(pts,-onePointTwo(1,:), 'k-'); hold on
p2 = loglog(pts,kimkarrila(1.2,pts), 'k-.');
p3 = loglog(pts,gms(1,1.2,pts), 'k.');

p4 = loglog(pts,-two(1,:), 'b-');
p5 = loglog(pts,kimkarrila(2,pts), 'b-.');
p6 = loglog(pts,gms(1,2,pts), 'b.');

p7 = loglog(pts,-five(1,:), 'r-');
p8 = loglog(pts,kimkarrila(5,pts), 'r-.');
p9 = loglog(pts,gms(1,5,pts), 'r.');

p10 = loglog(pts,-twelve(1,:), '-','Color',[0,0.7,0.9]);
p11 = loglog(pts,kimkarrila(12,pts), '-.', 'Color',[0,0.7,0.9]);
p12 = loglog(pts,gms(1,12,pts),':', 'Color',[0,0.7,0.9]);

hold off

xlim([2e-3 1e2]); ylim([0 1e3])
grid on
ylabel('$F/ 6 \pi\mu r_1 U$','interpreter','latex','fontsize',22);
xlabel('$h/r_1$','interpreter','latex','fontsize',22);
l=legend([p1 p4 p7 p10], '$r_2/r_1=1.2$','$r_2/r_1=2$','$r_2/r_1=5$','$r_2/r_1=12$');
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
set(l, 'interpreter','latex','fontsize',24, 'location','best');
title({'Nondimensional Normal Force'},'interpreter', 'latex', 'fontsize',24);

shg


% b %

figure
p2 = loglog(pts,abs(kimkarrila(1.2,pts)+onePointTwo(1,:)')./abs(onePointTwo(1,:)'), 'k-.'); hold on
p3 = loglog(pts,abs(gms(1,1.2,pts)+onePointTwo(1,:)')./abs(onePointTwo(1,:)'), 'k.'); 

p5 = loglog(pts,abs(two(1,:)'+kimkarrila(2,pts))./abs(two(1,:)'), 'b-.');
p6 = loglog(pts,abs(two(1,:)'+gms(1,2,pts))./abs(two(1,:)'), 'b.');

p8 = loglog(pts,abs(five(1,:)'+kimkarrila(5,pts))./abs(five(1,:)'), 'r-.');
p9 = loglog(pts,abs(five(1,:)'+gms(1,5,pts))./abs(five(1,:)'), 'r.');

p11 = loglog(pts,abs(twelve(1,:)'+kimkarrila(12,pts))./abs(twelve(1,:)'), '-.', 'Color',[0,0.7,0.9]);
p12 = loglog(pts,abs(twelve(1,:)'+gms(1,12,pts))./abs(twelve(1,:)'), ':', 'Color',[0,0.7,0.9]);

hold off

xlim([1e-2 1e2]); 
grid on
ylabel('Error','interpreter','latex','fontsize',22);
xlabel('$h/r_1$','interpreter','latex','fontsize',22);
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
title({'Relative Error'},'interpreter', 'latex', 'fontsize',24);

shg
