
close all force
clear all

set(0,'defaultaxesfontsize',20);
set(0,'defaulttextfontsize',20);
set(0,'defaultaxeslinewidth',1.5);
set(0,'defaultlinelinewidth',1.5);

% Figure 8a %
figure
TestPositiveDefinite2Particle(1e-3,1e2,3,200,.5,1e-12,'plot');

% Figure 8b %
figure
TestPositiveDefinite3Particle(1.01,100,3,200,.5,1e-12,'plot');