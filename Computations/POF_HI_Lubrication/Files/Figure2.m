% Figure 2 % 
close all force
clear all

set(0,'defaultaxesfontsize',20);
set(0,'defaulttextfontsize',20);
set(0,'defaultaxeslinewidth',1.5);
set(0,'defaultlinelinewidth',1.5);

% a %

ymin = 1e-3; N = 101;
geometricL = .25; pts = genForcePts(1e-3,N,geometricL);

r1 = 1/2; r2 = 1/2;
evalpts = pts + r1 + r2;
toleta = 1e-12;

F1F2 = zeros(2,N);

for i=1:N
    [FSphere1, FSphere2] = makeNormalScalar(evalpts(i),2*r2,toleta);
    F1F2(:,i)=[FSphere1;FSphere2];
end

geom.N = N; geom.L = geometricL; geom.yMin = ymin;
aHalfinfiniteSpectralLine = HalfInfSpectralLine(geom);
aHalfinfiniteSpectralLine.ComputeAll();
aHalfinfiniteSpectralLine.ComputeInterpolationMatrix([-1:.001:1]',true);

fig1 = figure(1);

l1 = loglog(pts,-F1F2(1,:), 'k-'); hold on
l2 = loglog(pts, gms(r1,r2,pts), 'k-.');
l3 = loglog(pts, kimkarrila(1,pts), 'b-.');

grid on

xlim([1e-3,1e2]); ylim([1e-2,1e3])
ylabel('$F/ 6 \pi\mu r_1 U$','interpreter','latex','fontsize',22);
xlabel('$h/r_1$','interpreter','latex','fontsize',22);
l=legend([l1 l2 l3],'$F_z^1$','$F_z^e$','$F_{z,l}$');
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
set(l, 'interpreter','latex','fontsize',24, 'location','southwest');
title({'Nondimensional Normal Force'},'interpreter', 'latex', 'fontsize',24);
shg

fig2 = figure(2);

ll1 = loglog(pts,-F1F2(1,:), 'k-');
hold on
ll2 = loglog(pts,gms(r1,r2,pts), 'k-.');
hold on
ll3 = loglog(pts, kimkarrila(1,pts), 'b-.');
hold on

grid on

xlim([2e-3,2.5e-3]); ylim([1e2,1.3e2])
l=legend([ll1 ll2 ll3],'$F_z^1$','$F_z^e$','$F_{z,l}$');
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
set(l, 'interpreter','latex','fontsize',24, 'location','best');

shg

[h_m h_i]=inset(fig1,fig2);

clear all

% b %

N = 200;
pts = genForcePts(1e-3,N,1);
toleta = 1e-12;

%  beta = 1.2  %

r1 = 1/2; r2 = 1.2/2;
evalpts = pts + r1 + r2 ;

beta = 2*r2;
F1F2 = zeros(2,N);

for i=1:N
    [FSphere1, FSphere2] = makeNormalScalar(evalpts(i),beta,toleta);
    F1F2(:,i)=[FSphere1;FSphere2];
end

onePointTwo = F1F2;

%  beta = 5  %

r1 = 1/2; r2 = 5/2;
evalpts = pts + r1 + r2 ;

beta = 2*r2;
F1F2 = zeros(2,N);
for i=1:N
    [FSphere1, FSphere2] = makeNormalScalar(evalpts(i),beta,toleta);
    F1F2(:,i)=[FSphere1;FSphere2];
end
five = F1F2;

%  beta = 12  %

r1 = 1/2; r2 = 12/2;
evalpts = pts + r1 + r2 ;

beta = 2*r2;
F1F2 = zeros(2,N);
for i=1:N
    [FSphere1, FSphere2] = makeNormalScalar(evalpts(i),beta,toleta);
    F1F2(:,i)=[FSphere1;FSphere2];
end
twelve = F1F2;

figure
p1 = loglog(pts,-onePointTwo(1,:), 'k--'); hold on
p2 = loglog(pts,1.2*onePointTwo(2,:), 'm--');

p5 = loglog(pts,-five(1,:), 'k-.');
p6 = loglog(pts,5*five(2,:), 'm-.');

p7 = loglog(pts,-twelve(1,:), 'k.');
p8 = loglog(pts,12*twelve(2,:), 'm.');

hold off

xlim([0 1e2]); ylim([0 1e3])
grid on
shg

ylabel('$|F_z^j|/ 6 \pi\mu r_1 U$','interpreter','latex','fontsize',22);
xlabel('$h/r_1$','interpreter','latex','fontsize',22);
l=legend([p1 p5 p7], '$r_2/r_1=1.2$','$r_2/r_1=5$', '$r_2/r_1=12$');
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
set(l, 'interpreter','latex','fontsize',24, 'location','best');
title({'Nondimensional Normal Force'},'interpreter', 'latex', 'fontsize',24);
