function eigenvalues = TestPositiveDefinite2Particle(hMin,hMax,dim,numberOfCentrePts,L,tol,varargin)
fs = 24;
lambda = 1; sigmaH = 1;
r1 = 1/2; r2 = lambda/2;

geom.N = numberOfCentrePts; geom.yMin = hMin; geom.yMax = hMax; geom.L = L;
aline = SpectralLine(geom);
aline.ComputeAll();
evalpts = aline.Pts.y + r1 + r2 ;

pp = linspace(-1,1,1e3)';
aline.ComputeInterpolationMatrix(pp,true);


eigenvaluesOfKGMS = [];

for i = 1:length(evalpts)
    x = [0;0;0;0;0;(evalpts(i))];
    KGMS = makeGMS(x,sigmaH,dim,tol);
    eigenvaluesOfKGMS = [eigenvaluesOfKGMS, eig(KGMS)];
end

% eigenvaluesOfKGMS

eigenvaluesOfKJO = [];
for i = 1:length(evalpts)
    x = [0;0;0;0;0;(evalpts(i))];
    KJO=makeJO(x,sigmaH,dim);
    eigenvaluesOfKJO = [eigenvaluesOfKJO, eig(KJO)];
end

% eigenvaluesOfKJO

eigenvaluesOfKJOarb = [];
for i = 1:length(evalpts)
    x = [0;0;0;0;0;(evalpts(i))];
    KJOarb=makeJOarb(x,sigmaH,dim);
    eigenvaluesOfKJOarb = [eigenvaluesOfKJOarb, eig(KJOarb)];
end

% eigenvaluesOfKJOarb

eigenvalues = [eigenvaluesOfKGMS; eigenvaluesOfKJO; eigenvaluesOfKJOarb];

fig1 = figure(1);
p1 = loglog(evalpts, eigenvaluesOfKGMS(1,:), 'k^--'); hold on
     loglog(evalpts, eigenvaluesOfKGMS(2,:), 'k^--');
     loglog(evalpts, eigenvaluesOfKGMS(3,:), 'k^--');
     loglog(evalpts, eigenvaluesOfKGMS(4,:), 'kO--');
     loglog(evalpts, eigenvaluesOfKGMS(5,:), 'kO--');
     loglog(evalpts, eigenvaluesOfKGMS(6,:), 'k-');

p2 = loglog(evalpts, eigenvaluesOfKJO(1,:), 'b^--'); 
     loglog(evalpts, eigenvaluesOfKJO(2,:), 'b^--'); 
     loglog(evalpts, eigenvaluesOfKJO(3,:), 'b^--'); 
     loglog(evalpts, eigenvaluesOfKJO(4,:), 'bO--'); 
     loglog(evalpts, eigenvaluesOfKJO(5,:), 'bO--'); 
     loglog(evalpts, eigenvaluesOfKJO(6,:), 'b-'); 

p3 = loglog(evalpts, eigenvaluesOfKJOarb(1,:), 'r^--');
     loglog(evalpts, eigenvaluesOfKJOarb(2,:), 'r^--');
     loglog(evalpts, eigenvaluesOfKJOarb(3,:), 'r^--');
	 loglog(evalpts, eigenvaluesOfKJOarb(4,:), 'rO--');
	 loglog(evalpts, eigenvaluesOfKJOarb(5,:), 'rO--');
	 loglog(evalpts, eigenvaluesOfKJOarb(6,:), 'r-');

ylim([1e-1 5e1]);
xlim([evalpts(1) 1e2]);
title('Eigenvalues of $\bf{R}$','interpreter', 'latex', 'fontsize', fs)
xlabel('$d/\sigma$','interpreter', 'latex', 'fontsize', fs-4)
ylabel('$\lambda_i$','interpreter', 'latex', 'fontsize', fs-4)

l=legend([p1 p2 p3], 'GMS','$X^A_{11}$ (3.17), $Y^A_{11}$ (4.15)','$X^A_{11}$ (3.20), $Y^A_{11}$ (4.19)');
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
set(l, 'interpreter','latex','fontsize',fs-4, 'location','best');

grid on

fig2 = figure(2);

p1 = semilogy(evalpts, eigenvaluesOfKGMS(1,:), 'k^--'); hold on
     semilogy(evalpts, eigenvaluesOfKGMS(2,:), 'k^--');
     semilogy(evalpts, eigenvaluesOfKGMS(3,:), 'k^--');
     semilogy(evalpts, eigenvaluesOfKGMS(4,:), 'kO--');
     semilogy(evalpts, eigenvaluesOfKGMS(5,:), 'kO--');
     semilogy(evalpts, eigenvaluesOfKGMS(6,:), 'k-');

p2 = semilogy(evalpts, eigenvaluesOfKJO(1,:), 'b^--'); 
     semilogy(evalpts, eigenvaluesOfKJO(2,:), 'b^--'); 
     semilogy(evalpts, eigenvaluesOfKJO(3,:), 'b^--'); 
     semilogy(evalpts, eigenvaluesOfKJO(4,:), 'bO--'); 
     semilogy(evalpts, eigenvaluesOfKJO(5,:), 'bO--'); 
     semilogy(evalpts, eigenvaluesOfKJO(6,:), 'b-'); 

p3 = semilogy(evalpts, eigenvaluesOfKJOarb(1,:), 'r^--');
     semilogy(evalpts, eigenvaluesOfKJOarb(2,:), 'r^--');
     semilogy(evalpts, eigenvaluesOfKJOarb(3,:), 'r^--');
	 semilogy(evalpts, eigenvaluesOfKJOarb(4,:), 'rO--');
	 semilogy(evalpts, eigenvaluesOfKJOarb(5,:), 'rO--');
	 semilogy(evalpts, eigenvaluesOfKJOarb(6,:), 'r-');

ylim([1e-1 5.2e2]);
xlim([evalpts(1) 1.1]);

grid on

set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')


[h_m, h_i]=inset(fig1,fig2);

shg



end