% getCentres3Sphere gives back a vector of centres for a 3-system confined
% to the plane z = 0. Two spheres are fixed at (0,dMin/2,0) and
% (0,-dMin/2,0) and the third sphere varies along x.

% INPUTS dMin - smallest centre to centre distance for the two fixed spheres
%        dMax - largest distance from the third sphere to the line
%        connecting the two sphere axis.
%        numberOfCentrePts - number of evaluation points
%        L - geometrical parameter for SpectralLine
%        varargin - plot grid for centres3Sphere or not

% OUTPUTS centres3Sphere - sphere centre evaluation points

% Rory Mills March 2020

function centres3Sphere = getCentres3Sphere(dMin,dMax,sigmaH,numberOfCentrePts,L,varargin)

if sigmaH>dMin
    disp('Error sigmaH must be smaller than fixed centre distance')
    return
end

x1 = [0;dMin/2;0];
x2 = [0;-dMin/2;0];
x3 = zeros(3,numberOfCentrePts);

geom.N = numberOfCentrePts; geom.L = L; geom.yMin = sqrt(3)/2*dMin; geom.yMax = dMax;

aline = SpectralLine(geom);
aline.ComputeAll();

thirdCentre = aline.Pts.y;
x3(1,:) = thirdCentre;
centres3Sphere = repelem([x1;x2],1,numberOfCentrePts);

centres3Sphere = [centres3Sphere;x3];

if strcmp(varargin,'plot');
    figure;
    aline.PlotGrid(); hold on
    plot(x1(1),x1(2),'o','MarkerEdgeColor','k','MarkerFaceColor','g');
    plot(x2(1),x2(2),'o','MarkerEdgeColor','k','MarkerFaceColor','g'); hold off
    shg   
end

end