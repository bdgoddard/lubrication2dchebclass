function [I] = c1c2(beta,N)

geom.N = N;
geom.yMin = 1;
geom.L = .1;
aLine = HalfInfSpectralLine(geom);
aLine.ComputeAll;
aLine.ComputeInterpolationMatrix([-1:0.01:1]',true);
x = aLine.Pts.y;


    function f = lnumerator(x)
        f = -beta^2 + ...
            beta ^2*(2*x.^2 + 2*x + 1).*exp(2*(beta +2)*x/beta) ...
            - exp(2*x/beta).* (beta^2 + 2*x.^2 -2*beta*x) ...
            + exp(2*(beta +1)*x/beta) ...
            .*(beta^2 +4*(beta +1)*x.^3 + 2*(beta +1)^2*x.^2 + 2*beta*(beta +1)*x);
    end

    function g = ldenominator(x)
        g = beta^2 - 2*exp((2*(beta+1)*x)/beta ).*(beta^2 +2*(beta +1)^2 * x.^2 ) ...
            + beta^2*exp(4*(beta +1)*x/beta);
    end

l = lnumerator(x)./ldenominator(x); 
l(isnan(l)) = 0;

k = l - 6*beta^3/((1+beta)^3) * x.^(-3);

intk = aLine.Int*k;

clear geom x aLine

geom.N = N;
geom.yMin = 1e-2;
geom.yMax = 1;
aLine = SpectralLine(geom);
aLine.ComputeAll;
aLine.ComputeInterpolationMatrix([-1:0.01:1]',true);
x = aLine.Pts.y;

l = lnumerator(x)./ldenominator(x); 
j = l - 6*beta^3/((1+beta)^3) * x.^(-3)  -6/5*beta*(1+7*beta+beta^2)/(1+beta)^3*x.^(-1);

j(isnan(j)) = 0; j(isinf(j)) = 0;

intj = aLine.Int*j;

I = 2/3*(intj+intk);

end