%% Jeffrey-Onishi 1984 
% FUNCTION AX11 TO COMPUTE THE ORDER 1 TERM AY11 (4.17) using 11 f_m's
function s = AY11(lambda)

l = lambda;

f = fMatrix();
lP = lambdaPowers(l);
onePluslP = onePlusLambdaPowers(l);

inversePowersOfTwo = 2.^([0:-1:-11]');

s1 = (f*lP).*onePluslP;
s1 = s1.*inversePowersOfTwo;

s2 = -2*g2(l)*[0; 1./([1:11]')];

s3 = 4*g3(l)*[0; 1./([1:11]')].*((m1([0:1:11]')).^(-1));

% s2 = -2*1^([1:-1:-11]')*g2(l);
% s3 = 4*g3(l)*1^([1:-1:-11]').*((m1([1:1:11]')).^(-1));
s = 0;
s = s + s1 + s2 + s3;

s = s(3:2:end);
s = 1 + sum(s);

% fEvenOnly = f(1:2:end,:);
% lPEvenOnly = lP(1:2:end);
% onePluslPEvenOnly = onePluslP(1:2:end);
% evenPowersOfTwo = 2.^([0:-2:-10]');

% 
% 
% AX11TEMP = f*AX11SUMMANDl(lambda); ax11tempeval = AX11TEMP(3:2:end);
% 
% ax11sqeval = (AX11SUMMANDQUOTIENT(lambda)); ax11sqeval = ax11sqeval(2:end);
% 
% ax11tempeval = ax11tempeval.*ax11sqeval;
% 
% s1 = 2.^(-(2:2:14))*ax11tempeval -7*1/4;
% 
% s1 = s1 - 2*((2:2:14).^(-1))*ones(7,1)*g2(lambda);
% 
% s1 = s1 + 4*((2:2:14).^(-1))*(m1(2:2:14).^(-1)')*g3(lambda);
% 
% A = 1 - 1/4*g1(lambda) + s1;
% 
    function z = m1(m) 
       z = -2*eq(m,2) + (m-2).*(1-eq(m,2));
    end

    function z = g2(l)
       z = 4/15*l.*(2 + l + 2*l.^2)./(1+l).^3;
    end

    function z = g3(l) 
        z =  2/375*(16-45.*l+58.*l.^2-45.*l.^3+16.*l.^4)./(1+l)^3;
    end

    function f = fMatrix()
        f(1,1) = 1; f(2,2) = 3/2; f(3,2) = 9/4; f(4,2:4) = [2,27/8,2]; 
        f(5,2:4) = [6,81/16,18]; f(6,3:5) = [63/2,243/32,63/2]; f(7,2:6) = [4,54,1241/64,81,72];
        f(8,3:7) = [144,1053/8,19083/128,1053/8,144];
        f(9,3:8) = [279,4261/8,126369/256,-117/8,648,288];
        f(10,3:9) = [576,1134,60443/32,766179/512,60443/32,1134,576];
        f(11,3:10) = [1152,7857/4,98487/16,10548393/1024,67617/8,-351/2,3888,1152];
        f(12,3:11) = [2304,7128,22071/2,2744505/128,95203835/2048,2744505/128,22071/2,7128,2304];

    end

    function lP = lambdaPowers(l)
        lP = [1; l; l.^2; l.^3; l.^4; l.^5; l.^6; l.^7; l.^8; l.^9; l.^10];

    end

    function onePluslP = onePlusLambdaPowers(l)
        onePluslP = [1; 1./(1+l); 1./(1+l).^2 ;1./(1+l).^3 ;1./(1+l).^4;...
                    1./(1+l).^5; 1./(1+l).^6; 1./(1+l).^7; 1./(1+l).^8;
                    1./(1+l).^9; 1./(1+l).^10; 1./(1+l).^11];
    end



end