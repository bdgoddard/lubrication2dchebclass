function eigenvalues = TestPositiveDefinite3Particle(dMin,dMax,dimension,numberOfCentrePts,L,tol,varargin)

lambda = 1;
sigmaH = 1;
dim = dimension;
r1 = 1/2;
r2 = lambda/2;

% evalpts = aline.Pts.y + r1 + r2;
% pp = linspace(-1,1,1e3)';
% aline.ComputeInterpolationMatrix(pp,true);

centres3Sphere = getCentres3Sphere(dMin,dMax,sigmaH,numberOfCentrePts,L);
% centres3Sphere(7,:)
% pause
eigenvaluesOfKGMS = [];
for i = 1:size(centres3Sphere,2)
    x = centres3Sphere(:,i);
    KGMS = makeGMS(x,sigmaH,dim,tol);
    eigenvaluesOfKGMS = [eigenvaluesOfKGMS, eig(KGMS)];
end

eigenvaluesOfKJO = [];
for i = 1:size(centres3Sphere,2)
    x = centres3Sphere(:,i);
    KJO=makeJO(x,sigmaH,dim);
    eigenvaluesOfKJO = [eigenvaluesOfKJO, eig(KJO)];
end


eigenvaluesOfKJOarb = [];
for i = 1:size(centres3Sphere,2)
    x = centres3Sphere(:,i);
    KJOarb=makeJOarb(x,sigmaH,dim);
    eigenvaluesOfKJOarb = [eigenvaluesOfKJOarb, eig(KJOarb)];
end
eigenvalues = [eigenvaluesOfKGMS;eigenvaluesOfKJO;eigenvaluesOfKJOarb];
% eigenvalues = [eigenvaluesOfKGMS];

% eigenvaluesOfKGMS
% eigenvaluesOfKJO
% eigenvaluesOfKJOarb

if strcmp(varargin,'plot');
    set(0,'defaultaxesfontsize',20);
    set(0,'defaulttextfontsize',20);
    set(0,'defaultaxeslinewidth',1.5);
    set(0,'defaultlinelinewidth',1.5);
    
    fs = 24;
    
    evalpts = centres3Sphere(7,:);
    fig1 = figure(1);
    p1 = loglog(evalpts, eigenvaluesOfKGMS(1,:), 'k^--'); hold on
    loglog(evalpts, eigenvaluesOfKGMS(2,:), 'k^--');
    loglog(evalpts, eigenvaluesOfKGMS(3,:), 'k^--');
    loglog(evalpts, eigenvaluesOfKGMS(4,:), 'k^--');
    loglog(evalpts, eigenvaluesOfKGMS(5,:), 'k^--');
    loglog(evalpts, eigenvaluesOfKGMS(6,:), 'k^--');
    loglog(evalpts, eigenvaluesOfKGMS(7,:), 'kO--');
    loglog(evalpts, eigenvaluesOfKGMS(8,:), 'kO--');
    loglog(evalpts, eigenvaluesOfKGMS(9,:), 'k-');
    
    p2 = loglog(evalpts, eigenvaluesOfKJO(1,:), 'b^--');
    loglog(evalpts, eigenvaluesOfKJO(2,:), 'b^--');
    loglog(evalpts, eigenvaluesOfKJO(3,:), 'b^--');
    loglog(evalpts, eigenvaluesOfKJO(4,:), 'b^--');
    loglog(evalpts, eigenvaluesOfKJO(5,:), 'b^--');
    loglog(evalpts, eigenvaluesOfKJO(6,:), 'b^--');
    loglog(evalpts, eigenvaluesOfKJO(7,:), 'bO--');
    loglog(evalpts, eigenvaluesOfKJO(8,:), 'bO--');
    loglog(evalpts, eigenvaluesOfKJO(9,:), 'b-');
    %
    p3 = loglog(evalpts, eigenvaluesOfKJOarb(1,:), 'r^--');
    loglog(evalpts, eigenvaluesOfKJOarb(2,:), 'r^--');
    loglog(evalpts, eigenvaluesOfKJOarb(3,:), 'r^--');
    loglog(evalpts, eigenvaluesOfKJOarb(4,:), 'r^--');
    loglog(evalpts, eigenvaluesOfKJOarb(5,:), 'r^--');
    loglog(evalpts, eigenvaluesOfKJOarb(6,:), 'r^');
    loglog(evalpts, eigenvaluesOfKJOarb(7,:), 'rO--');
    loglog(evalpts, eigenvaluesOfKJOarb(8,:), 'rO--');
    loglog(evalpts, eigenvaluesOfKJOarb(9,:), 'r-');
    %
    
    %
    ylim([1e-1 1e2]);
    xlim([evalpts(1) 1e2]);
    title('Eigenvalues of $\bf{R}$','interpreter', 'latex', 'fontsize', fs)
    xlabel('$d/\sigma$','interpreter', 'latex', 'fontsize', fs-4)
    ylabel('$\lambda_i$','interpreter', 'latex', 'fontsize', fs-4)
    
    l=legend([p1 p2 p3], 'GMS','$X^A_{11}$ (3.17), $Y^A_{11}$ (4.15)','$X^A_{11}$ (3.20), $Y^A_{11}$ (4.19)');
    set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
    set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
    set(l, 'interpreter','latex','fontsize',fs-4, 'location','best');
    
    grid on
    
    fig2 = figure(2);
    
    p1 = loglog(evalpts, eigenvaluesOfKGMS(1,:), 'k^--'); hold on
    loglog(evalpts, eigenvaluesOfKGMS(2,:), 'k^--');
    loglog(evalpts, eigenvaluesOfKGMS(3,:), 'k^--');
    loglog(evalpts, eigenvaluesOfKGMS(4,:), 'k^--');
    loglog(evalpts, eigenvaluesOfKGMS(5,:), 'k^--');
    loglog(evalpts, eigenvaluesOfKGMS(6,:), 'k^--');
    loglog(evalpts, eigenvaluesOfKGMS(7,:), 'kO--');
    loglog(evalpts, eigenvaluesOfKGMS(8,:), 'kO--');
    loglog(evalpts, eigenvaluesOfKGMS(9,:), 'k-');
    
    p2 = loglog(evalpts, eigenvaluesOfKJO(1,:), 'b^--');
    loglog(evalpts, eigenvaluesOfKJO(2,:), 'b^--');
    loglog(evalpts, eigenvaluesOfKJO(3,:), 'b^--');
    loglog(evalpts, eigenvaluesOfKJO(4,:), 'b^--');
    loglog(evalpts, eigenvaluesOfKJO(5,:), 'b^--');
    loglog(evalpts, eigenvaluesOfKJO(6,:), 'b^--');
    loglog(evalpts, eigenvaluesOfKJO(7,:), 'bO--');
    loglog(evalpts, eigenvaluesOfKJO(8,:), 'bO--');
    loglog(evalpts, eigenvaluesOfKJO(9,:), 'b-');
    %
    p3 = loglog(evalpts, eigenvaluesOfKJOarb(1,:), 'r^--');
    loglog(evalpts, eigenvaluesOfKJOarb(2,:), 'r^--');
    loglog(evalpts, eigenvaluesOfKJOarb(3,:), 'r^--');
    loglog(evalpts, eigenvaluesOfKJOarb(4,:), 'r^--');
    loglog(evalpts, eigenvaluesOfKJOarb(5,:), 'r^--');
    loglog(evalpts, eigenvaluesOfKJOarb(6,:), 'r^--');
    loglog(evalpts, eigenvaluesOfKJOarb(7,:), 'rO--');
    loglog(evalpts, eigenvaluesOfKJOarb(8,:), 'rO--');
    loglog(evalpts, eigenvaluesOfKJOarb(9,:), 'r-');
    %
    
    ylim([1e-1 1e2]);
    xlim([.87 .95]);
    %
    grid on
    %
    % xlim([2e-3,2.5e-3]); ylim([1e2,1.3e2])
    % l=legend([ll1 ll2 ll3 ll4],'$\mathcal{F}_z^1$','$\mathcal{F}_z^\ast$','$F_z$','Interpolant');
    set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
    set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
    % set(l, 'interpreter','latex','fontsize',24, 'location','best');
    
    shg
    
    [h_m, h_i]=inset(fig1,fig2);
    
end


% tol
% nbody
end
% % set(0,'defaultaxesfontsize',20);
% % set(0,'defaulttextfontsize',20);
% % set(0,'defaultaxeslinewidth',1.5);
% % set(0,'defaultlinelinewidth',1.5);
%
% fs = 24;
% geom.N = 200; geom.yMin = 1e-3; geom.yMax = 100; geom.L = .5;
% aline = SpectralLine(geom);
% aline.ComputeAll();
% evalpts = aline.Pts.y + r1 + r2 ;
% % aline.PlotGrid();
%
% pp = linspace(-1,1,1e3)';
% aline.ComputeInterpolationMatrix(pp,true);
% % aline.PlotGrid();
%
% %%
% figure
% p1 = loglog(evalpts, eigenvaluesOfKGMS(1,:), 'k-'); hold on
%      loglog(evalpts, eigenvaluesOfKGMS(2,:), 'k-');
%      loglog(evalpts, eigenvaluesOfKGMS(3,:), 'k-');
%      loglog(evalpts, eigenvaluesOfKGMS(4,:), 'k-');
%      loglog(evalpts, eigenvaluesOfKGMS(5,:), 'k-');
%      loglog(evalpts, eigenvaluesOfKGMS(6,:), 'k-');
%
% p2 = loglog(evalpts, eigenvaluesOfKJO(1,:), 'b-');
%      loglog(evalpts, eigenvaluesOfKJO(2,:), 'b-');
%      loglog(evalpts, eigenvaluesOfKJO(3,:), 'b-');
%      loglog(evalpts, eigenvaluesOfKJO(4,:), 'b-');
%      loglog(evalpts, eigenvaluesOfKJO(5,:), 'b-');
%      loglog(evalpts, eigenvaluesOfKJO(6,:), 'b-');
%
% p3 = loglog(evalpts, eigenvaluesOfKJOarb(1,:), 'r-');
%      loglog(evalpts, eigenvaluesOfKJOarb(2,:), 'r-');
%      loglog(evalpts, eigenvaluesOfKJOarb(3,:), 'r-');
% 	 loglog(evalpts, eigenvaluesOfKJOarb(4,:), 'r-');
% 	 loglog(evalpts, eigenvaluesOfKJOarb(5,:), 'r-');
% 	 loglog(evalpts, eigenvaluesOfKJOarb(6,:), 'r-');
%
% ylim([1e-1 5e1]);
% xlim([evalpts(1) 1e2]);
% title('Eigenvalues of $\bf{R}$','interpreter', 'latex', 'fontsize', fs)
% xlabel('$d/r_1$','interpreter', 'latex', 'fontsize', fs-4)
% ylabel('$\lambda_i$','interpreter', 'latex', 'fontsize', fs-4)
%
% l=legend([p1 p2 p3], 'GMS','$X^A_{11}$ (3.17), $Y^A_{11}$ (4.15)','$X^A_{11}$ (3.20), $Y^A_{11}$ (4.19)');
% set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
% set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
% set(l, 'interpreter','latex','fontsize',fs-4, 'location','best');
%
% grid on
%
% shg
%%
% figure
% tempeigs = [eigenvaluesOfKGMS(1,:),1]';
% aline.plot(tempeigs)
%




