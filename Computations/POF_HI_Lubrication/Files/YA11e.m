%% Jeffrey-Onishi 1984 
% FUNCTION TO COMPUTE EXPANSION OF XA11 (4.15)
function Y = YA11e(lambda,d)

r2 = lambda/2;
r1 = 1/2;
h = d-r2-r1;

s = 2*(1+h/(1+lambda));
x = s-2;


g2 = @(l) 4/15*l.*(2 + l + 2*l.^2)./(1+l).^3;
g3 = @(l) 2/375*(16 -45*l + 58*l.^2 -45*l.^3 + 16*l.^4)./(1+l).^3;


Y =  g2(lambda)*log(x.^(-1)) + g3(lambda)*x.*log(x.^(-1));
Y = 2*Y;
Y = Y + AY11(lambda);
end