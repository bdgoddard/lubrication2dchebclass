function fe = gms(r1,r2,h)
N = 1e3;
beta = r2/r1;
fe = (beta^2/(1+beta)^2./((h))+1/5*beta*(1+7*beta+beta^2)/(1+beta)^3*log(1./((h))))...
    +2/5*beta*(1+7*beta+beta^2)/(1+beta)^3*(double(eulergamma)+1/2+2*log(2)+1/2*log(2*beta/(1+beta))) + 1/15/2/(1+beta)^3*(-120*beta^2+10*beta^3) + 1/2*c1c2(beta,N);
end