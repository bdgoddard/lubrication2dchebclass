%% Jeffrey-Onishi 1984 
% FUNCTION TO COMPUTE EXPANSION OF XA11 (3.17)
function X = XA11e(lambda,d)

r2 = lambda/2;
r1 = 1/2;

h = d-r1-r2;

s = 2*(1+h/(1+lambda));
x = s-2;

g1 = @(l) 2*l.^2./(1+l).^3;
g2 = @(l) 1/5*l.*(1 + 7*l + l.^2)./(1+l).^3;
g3 = @(l) 1/42*(1 + 18*l - 29*l.^2 + 18*l.^3 + l.^4)./(1+l).^3;

X = g1(lambda)*x.^(-1) + g2(lambda)*log(x.^(-1)) + g3(lambda)*x.*log(x.^(-1));

X = X + AX11(lambda);

end