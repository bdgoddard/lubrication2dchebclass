% makeRegularPacking gives back a vector of particle centres for a regular sphere
% packing with a specified centre-centre distance.

% sigma = sphere diameter
% centredistance = centre to centre distance
% dim = dimension: 1d line/ 2d planar/ 3d volume
% numberOfRepetions = number of repetitions of the unit
% varagin = 'plot'

% Rory Mills February 2020

function [centres, packingFraction]= makeRegularPacking(sigma,centredistance,dim,numberOfRepetions,varargin)

% analytical packing fraction
packingFraction = 2*(pi/(3*sqrt(8)))*sigma/centredistance;

s = varargin;

if sigma>centredistance
    disp('Error sigma must be smaller than centre distance')
    return
end

switch dim
    case 1
        if ~isempty(varargin);
            disp('Geometry: 1D line. Computing regular sphere packing...')
        end
        
        centres1D = makeCentres1D();
        centres = centres1D;
        
        if strcmp(s,'plot');
            x = centres1D;
            y = zeros(size(x));
            figure; plot(x,y,'ro'); shg
        end
        
        return
        
    case 2
        if ~isempty(varargin);
            disp('Geometry: 2D planar. Computing regular sphere packing...')
        end
        
        centres2D = makeCentres2D();
        centres = centres2D;
        
        if strcmp(s,'plot');
            x = centres2D(1:3:end);
            y = centres2D(2:3:end);
            
            figure; plot(x,y,'ro'); shg
        end
        
        return
        
    case 3
        if ~isempty(varargin);
            disp('Geometry: 3D volume. Computing regular sphere packing...')
        end
        
        centres3D = makeCentres3D();
        centres = centres3D;
        
        if strcmp(s,'plot');
            x = centres3D(1:3:end);
            y = centres3D(2:3:end);
            z = centres3D(3:3:end);
            
            figure; plot3(x,y,z,'ro');
            shg
        end
        return
        
    otherwise
        disp('Nonphysical dimension. Dimension must be 3 or below.')
        centres = [];
        return
end

    function centres1D = makeCentres1D()
        centres1D = zeros(numberOfRepetions,1);
        
        for i = 2:numberOfRepetions
            centres1D(i) = centres1D(i-1) + centredistance;
        end
        
    end

    function centres2D = makeCentres2D()
        
        [unit] = makeUnit2D();
        [xBoolVec, yBoolVec, ~] = makeXYZBools();
        
        xunit = [];
        yunit = [];
        
        for i = 1:numberOfRepetions
            xunit = [xunit; unit];
            unit = unit + 2*centredistance*xBoolVec;
        end
        
        for j = 1:numberOfRepetions
            yunit = [yunit; xunit];
            xunit = xunit + 2*centredistance*sqrt(3)/2*yBoolVec;
        end
        
        centres2D = yunit;
        
    end

    function centres3D = makeCentres3D()
        
        yunit = makeUnit3D();
        ntemp = length(yunit)/dim;
        ztemp = repmat([0;0;1],ntemp,1);
        zunit = [];
        
        
        for j = 1:numberOfRepetions
            zunit = [zunit; yunit];
            yunit = yunit + sqrt(3)*centredistance*ztemp;
        end
        
        
        centres3D = zunit;
        
    end


    function [unit] = makeUnit3D()
        
        tempunit = makeCentres2D();
        
        ntemp = length(tempunit)/dim;
        xtemp = repmat([1;0;0],ntemp,1);
        ytemp = repmat([0;1;0],ntemp,1);
        ztemp = repmat([0;0;1],ntemp,1);
        
        unitUpdate = centredistance/2*xtemp...
            + sqrt(3)/4*centredistance*ytemp...
            + sqrt(3)/2*centredistance*ztemp;
        
        tempunit = [tempunit; tempunit+ unitUpdate];
        
        unit = tempunit;
        
    end

    function [unit] = makeUnit2D()
        
        unit = zeros(3*4,1);
        unit(1:3) = [0;0;0];
        unit(4:6) = [1;0;0];
        unit(7:9) = [1/2;sqrt(3)/2;0];
        unit(10:12) = [3/2;sqrt(3)/2;0];
        unit = unit*centredistance;
        
    end

    function [xBoolVec, yBoolVec, zBoolVec] = makeXYZBools()
        
        % x y z booleans
        xBoolVec = repmat([1;0;0],4,1);
        yBoolVec = repmat([0;1;0],4*numberOfRepetions,1);
        zBoolVec = repmat([0;0;1],4*numberOfRepetions*numberOfRepetions,1);
        
    end

end

