function pts=genForcePts(yMin,numPts,geometricL)
% GENFORCEPTS generates the force data points and transformed chebyshev points
% from a given ymin, L 

% Get instance of HalfInfSpectralLine class
Geom.N = numPts; Geom.L = geometricL; 
Geom.yMin = yMin;
S = HalfInfSpectralLine(Geom);

% Get the computational points
pts = S.Pts.y;
% ChebOutFile = strcat(filename,'CompPts.dat');
% csvwrite(ChebOutFile,CompPts)

% 
end