% Figure 11 %
%--------------------------------------------------------------------------
% compulsory physical constants
%--------------------------------------------------------------------------

% geometry
geom ='planar2D';
% dimension in which to do the DDFT calculations
stocDim=2;
DDFTDim=2;

% number of particles
nParticlesS = 50;

kBT=1;          % temperature
mS=1;           % particle mass
gammaS=2;       % friction coefficient
D0S=kBT./mS./gammaS;    % compute diffusion coefficient

%--------------------------------------------------------------------------
% V1 parameters
%--------------------------------------------------------------------------

V1DV1='V1_Well_Move_InfSpace';

% appropriate physical parameters for potentials in V1DV1
V0S = 0.01;
nAddS = 2;
V0addS = 3;
tauS = 0.01;
RS = 2.0;

sigma1AddS = 50;
sigma2AddS = 50;

y10aS       = -2;
y20aS       = 0;
y10bS       = 2;
y20bS       = 0;

% form into structure to make it easy to pass arbitrary parameters to
% potentials

potParamsNames = {'V0','V0add','tau','sigma1Add','sigma2Add',...
                  'y10a','y20a','y10b','y20b','nAdd'};

%--------------------------------------------------------------------------
% V2 parameters
%--------------------------------------------------------------------------

V2DV2='hardSphere';
sigmaS = 1;
potParams2Names={'sigma'};

%--------------------------------------------------------------------------
% HI parameters
%--------------------------------------------------------------------------
sigmaHS = 1;
cutOffOS = 2;

HIParamsNames={'sigmaH','cutOffO'};


%--------------------------------------------------------------------------
% Save time setup
%--------------------------------------------------------------------------

% end time of calculation
tMax=6*gammaS;

%--------------------------------------------------------------------------
% DDFT setup
%--------------------------------------------------------------------------

y1Plot=5;
y2Plot=5;

Phys_Area = struct('shape','InfSpace_FMT','y1Min',-inf,'y1Max',inf,'N',[20,20],'L1',12,...
                    'y2Min',-inf,'y2Max',inf,'L2',12);
                
Sub_Area = struct('shape','Box','y1Min',-3,'y1Max',3,'N',[20,20],...
                      'y2Min',0.5,'y2Max',1);
                   
Plot_Area = struct('y1Min',-y1Plot,'y1Max',y1Plot,'N1',100,...
                       'y2Min',-y2Plot,'y2Max',y2Plot,'N2',100);

Fex_NumRoth   = struct('Fex','FMTRoth',...
                       'Ncircle',20,'N1disc',20,'N2disc',20);
HI_None = [];            
                   
% HI_SR = struct('N',[20;20],'L',4,'HI11','GMS11_2D','HI12','GMS12_2D', ...
%                       'HIPreprocess', 'GMSPreprocess2D');                                                      

HI_SR = struct('N',[20;20],'L',4,'HI11','Kim_Karrila_11_2D','HI12','Kim_Karrila_12_2D', ...
                      'HIPreprocess', 'KKPreprocess2D');                                                      

% HI_SR = struct('N',[20;20],'L',4,'HI11','JOArb11_2D','HI12','JOArb12_2D', ...
%                       'HIPreprocess', 'GMSPreprocess2D');                                                      
                  

HIParamsNamesDDFT={'sigmaH','sigma', 'cutOffO'};                  
                  
eq_Num = struct('fsolve','fsolve');
                   
PhysArea = {Phys_Area, Phys_Area};

SubArea  = {Sub_Area, Sub_Area};

PlotArea = {Plot_Area, Plot_Area};

FexNum   = {Fex_NumRoth, Fex_NumRoth};

V2Num    = {[],[]};

eqNum    = {eq_Num,eq_Num};

HINum    = {HI_None, HI_SR };


DDFTCode = {'DDFTDynamics', 'DDFTDynamics'};
        
doPlots = false;

Inertial = {true,true};
% type of DDFT calculations, either 'rv' to include momentum, or 'r' for
% the standard position DDFT

DDFTType={'rv','rv'};


DDFTParamsNames = {{'PhysArea','SubArea','PlotArea','FexNum','V2Num','eqNum','HINum','doPlots','Inertial'}, ...
                   {'PhysArea','SubArea','PlotArea','FexNum','V2Num','eqNum','HINum','doPlots','Inertial'}};
             
               
% DDFTName={'No HI', 'With GMS'};
DDFTName={'No HI', 'With Kim & Karrila'};
% DDFTName={'No HI', 'With Jeffrey & Onishi'};

% whether to do DDFT calculations
doDDFT={true,true};

% do we load and save the DDFT data
loadDDFT={true,true};


% DDFTColour = {{'m'},{'k'}};
DDFTColour = {{'m'},{'b'}};
% DDFTColour = {{'m'},{'r'}};

%--------------------------------------------------------------------------
% Plotting setup
%--------------------------------------------------------------------------

plotType = 'surf';

viewPoint = [-5;25];

% x axis for position and velocity plots
rMin=[-y1Plot;-y2Plot];
rMax=[y1Plot;y2Plot];
pMin=rMin;
pMax=rMax;

% y axis for position and velocity plots
RMin=0;
RMax=0.5;

PMin=[-1;-1];
PMax=[1;1];

% y axis for mean position and velocity plots
RMMin=[0;-2];
RMMax=[1;2];
PMMin=[-1;-1];
PMMax=[1;1];

% number of bins for histograming of stochastic data
nBins=[20;20];

% determine which movies/plots to make
% distribution movies/plots
doMovieGif     = false;
doMovieAvi     = false;
doPdfs = false;
doInitialFinal = true;
doMeans        = false;
doEquilibria   = false;
doSnapshotsError = false;

sendEmail = false;
