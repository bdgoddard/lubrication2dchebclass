% Figure 9b %

close all force
clear all

set(0,'defaultaxesfontsize',20);
set(0,'defaulttextfontsize',20);
set(0,'defaultaxeslinewidth',1.5);
set(0,'defaultlinelinewidth',1.5);

fs = 24;

[mineigs, volFracPts] = computeEigsOfR(.95,.5,100,1,3,1e-12);

minEigOfKGMS = mineigs(:,1);
minEigOfKJO = mineigs(:,2);
minEigOfKJOarb = mineigs(:,3);

volFracPtsScaled = pi/(3*sqrt(2))*volFracPts;
volFracPtsScaled = 1./volFracPtsScaled;

figure
p3 = plot(volFracPtsScaled,minEigOfKJOarb,'r-'); hold on
p2 = plot(volFracPtsScaled,minEigOfKJO,'b-');
p1 = plot(volFracPtsScaled,minEigOfKGMS,'k-'); hold off

xlim([1/(pi/(3*sqrt(2))) volFracPtsScaled(1)+.1]);

title('Smallest Eigenvalue of $\bf{R}$','interpreter', 'latex', 'fontsize', fs)
xlabel('$\phi^{-1}$','interpreter', 'latex', 'fontsize', fs-4)
ylabel('$\lambda_{\min}$','interpreter', 'latex', 'fontsize', fs-4)

l=legend([p1 p2 p3], 'GMS','$X^A_{11}$ (3.17), $Y^A_{11}$ (4.15)','$X^A_{11}$ (3.20), $Y^A_{11}$ (4.19)');
set(gca, 'YMinorTick', 'on', 'YMinorGrid', 'off')
set(gca, 'XMinorTick', 'on', 'XMinorGrid', 'off')
set(l, 'interpreter','latex','fontsize',fs-4, 'location','best');

grid on

shg