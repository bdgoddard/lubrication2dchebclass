function [VBack_S,VAdd_S]=Quad1D(y,t,opts)
% V = alpha (y-y0)^2

    k1 = opts.k1;
    y0    = opts.y0;

    %--------------------------------------------------------------------------
    VBack  = k1 * (y-y0).^2;
    DVBack = 2*k1*(y-y0);
    %--------------------------------------------------------------------------    

    VAdd  = zeros(size(y));
    DVAdd = zeros(size(y));
        
    %--------------------------------------------------------------------------

    VBack_S = struct('V',VBack,'DV',DVBack);

    VAdd_S = struct('V',VAdd,'DV',DVAdd);

end