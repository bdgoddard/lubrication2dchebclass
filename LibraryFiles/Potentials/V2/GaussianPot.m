function V2 = GaussianPot(r,pot)
    ca = pot.Ca;
    cr = pot.Cr;
    la = pot.la; 
    lr = pot.lr;
    k2 = pot.k2;
    n = -1;
    V20 = 2^n;
    V2= k2*V20*(-ca*exp(-(r-1/4).^2/1e-2) + ca*exp(-(r+1/4).^2/1e-2));
end