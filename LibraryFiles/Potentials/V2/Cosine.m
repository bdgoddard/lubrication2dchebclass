function V2 = Cosine(x,y,pot)
%        TWO BODY MEAN ZERO FUNCTION: -kappa2*COS((x-y)/L*pi);
       L = pot.L;
       V2 = -(cos((x-y)*2*pi/L)); 

       if isfield(pot,'k2')
           V2 = pot.k2*V2;
       end
           
end