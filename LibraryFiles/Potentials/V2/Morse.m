function [z,dzdr_r,a] = Morse(r,optsPhys)

    params  = optsPhys.V2;
        
    
    z =params.k2*(-exp(-abs(r).^2));
    ztemp = 0;
%     ztemp = params.k2*params.Cr*exp(-abs(r)/params.lr);
    z = z+ztemp;
    
%     dzdr_r = (params.Ca/params.la*(1./abs(r))*exp(-abs(r)/params.la)...
%                 -params.Cr/params.lr*(1./abs(r))*exp(-abs(r)/params.lr));
    dzdr_r = []; % Not needed for ddft 


%     epsilon = optsPhys.epsilon;
%     alpha   = optsPhys.alpha;
%     
%     z = epsilon.*exp(-(r.^2)./(alpha.^2));
%     z(r == inf) = 0;
%         
%     dzdr_r = -2*epsilon./(alpha.^2).*exp(-(r.^2)./(alpha.^2));  
%     
%     a = pi*epsilon*alpha^2/2;
    a = 0;
end
