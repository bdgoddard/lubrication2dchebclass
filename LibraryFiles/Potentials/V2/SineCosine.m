function V2 = SineCosine(x,y,pot)
    V2 = pot.k2*sin(x).*sin(y);
end