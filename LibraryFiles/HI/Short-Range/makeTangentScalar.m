function [YA11, numberOfUsedSummationTerms] = makeTangentScalar(centreDistance,sigmaH,tol)

r2 = sigmaH/2;
r1  = 1/2;

x = centreDistance;
toleta = tol;
% eta1eta2 = Eta1Eta2(sigmaH/2,sigmaH/2,x,toleta);
% eta1eta2 = getEtas(sigmaH/2,sigmaH/2,x);
eta1eta2 = getEtas(r1,r2,x);
alpha = eta1eta2(1);
N = 100;
if isnan(alpha)
    alpha = 100;
end

[M,RHS] = makeLinearSystem(N);
AN = M\RHS;
YA11 = getTangentForce(AN);

YA11OLD = YA11;
change = 1;
convergenceTol = tol;
while change>convergenceTol
    N = N+5;
    [M,RHS] = makeLinearSystem(N);
    AN = M\RHS;
    YA11 = getTangentForce(AN);
    change = abs(YA11-YA11OLD);
    YA11OLD = YA11;
end

numberOfUsedSummationTerms = N;


    function [M,RHS] = makeLinearSystem(N)
        
        %construct recurrence matrix
        M = sparse(N,N);
        
        M(1,1:2) = [(2*1+1)-5*gamma(1,alpha)-1*(2*1-1)./(2*1+1).*(gamma(1-1,alpha)+1)+(1+1).*(2*1+3)./(2*1+1).*(gamma(1+1,alpha)-1),
            (1+2).*(2*1+5)./(2*1+3).*(gamma(1,alpha)+1)-(1+2).*(gamma(1+1,alpha)+1)];
        
        M(N,N-1:N) = [(N-1).*(gamma(N-1,alpha)-1)-(N-1).*(2*N-3)./(2*N-1).*(gamma(N,alpha)-1),
            (2*N+1)-5*gamma(N,alpha)-N*(2*N-1)./(2*N+1).*(gamma(N-1,alpha)+1)+(N+1).*(2*N+3)./(2*N+1).*(gamma(N+1,alpha)-1)];
        
        for n=2:N-1
            M(n,n-1:n+1)=[(n-1).*(gamma(n-1,alpha)-1)-(n-1).*(2*n-3)./(2*n-1).*(gamma(n,alpha)-1),
                (2*n+1)-5*gamma(n,alpha)-n*(2*n-1)./(2*n+1).*(gamma(n-1,alpha)+1)+(n+1).*(2*n+3)./(2*n+1).*(gamma(n+1,alpha)-1),
                (n+2).*(2*n+5)./(2*n+3).*(gamma(n,alpha)+1)-(n+2).*(gamma(n+1,alpha)+1)];
        end
        
        %contruct right hand side
        FR = @(n,alpha) sqrt(2)*exp(-(n+1/2)*alpha).*(exp(alpha).*csch((n-1/2)*alpha)-2*csch((n+1/2)*alpha)+...
            exp(-alpha).*csch((n+3/2)*alpha));
        
        RHS = zeros(N,1);
        for n=1:N
            RHS(n) =  FR(n,alpha);
        end
        
    end

    function YA11 = getTangentForce(AN)
        
        % make summation coefficients
        BN  = zeros(N,1); DN = zeros(N,1);
        
        DN(1) = 2*sqrt(2)*exp(-(1+1/2)*alpha).*csch((1+1/2)*alpha)+(2)*(3)./(5).*(gamma(1,alpha)+1)*AN(2);
        BN(1) = -2*gamma(1,alpha)*AN(1) + 2*3/5*(gamma(1,alpha)+1)*AN(2);
        
        DN0 = 2*sqrt(2)*exp(-(0+1/2)*alpha).*csch((0+1/2)*alpha)+(2)*(1)./(3).*(gamma(0,alpha)+1)*AN(1);
        
        for n=2:N-1
            DN(n) = 2*sqrt(2)*exp(-(n+1/2)*alpha).*csch((n+1/2)*alpha)-(n-1)*(n)./(2*n-1)*(gamma(n,alpha)-1).*AN(n-1)...
                +(n+2)*(n+1)./(2*n+3).*(gamma(n,alpha)+1)*AN(n+1);
            BN(n) = 2*(n-1)/(2*n-1)*(gamma(n,alpha)-1)*AN(n-1)-2*gamma(n,alpha)*AN(n)...
                +2*(n+2)/(2*n+3)*(gamma(n,alpha)+1)*AN(n+1);
            BN(n) = (n+1)*(n)*BN(n);
        end
        
        %sum the series
        BN(1) = 2*BN(1); % Remember summand is n(n+1)B_n...
        
        YA11 = sum(DN+BN)+DN0;
        
        YA11 = -1/6*sqrt(2)*sinh(alpha)*YA11;
        
%         % subtract off stokes drag
%         stokesindex = min(find(YA11==-1));
%         YA11(stokesindex:end)=-1;
%         YA11(isnan(YA11))=-1;
%         YA11 = YA11 + 1;
%         
        
    end

    function g = gamma(n,z)
        g = coth(z).*coth((n+1/2)*z);
    end


end