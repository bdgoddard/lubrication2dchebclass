function HI = Kim_Karrila_12_2D(x,y,optsPhys)

N = length(x);
id = IoxI(N);
rr = roxr(x,y);
beta = optsPhys.beta;

if beta ==1
    disp('Computing common species forces with Kim & Karrila...');
else
    disp('Computing cross species forces with Kim & Karrila...');
end

r1 = 1/2;
r2 = beta/2;

% r1 = 1; r2 = beta;
f1 = beta^2/(1+beta)^2;
f2 = 1/5*beta*(1 + 7*beta + beta^2)/(1+beta)^3;

g1 = 4/15*(2 + beta + 2*beta^2)/(1+beta)^3;

% convpts = (x.^2+y.^2).^(1/2);

rsInv = rsInverse(x,y);

% HI = 3/8*sigmaH*rInv.*(id + rr) + 1/16*sigmaH^3*rInv.^3.*(id-3*rr);
HI1 = f1*rsInv + f2*log(rsInv);
HI1 = -HI1;
HI1 = HI1.*rr;

HI2 = g1*log(rsInv);
HI2 = -HI2;
HI2 = HI2.*(id-rr);
HI = HI1 + HI2;

HI(isnan(rsInv) | rsInv == 0)=0;
HI(isnan(HI))=0;

HI = -HI;

function rr = roxr(x,y)
        % x and y are the kron products
        rr = zeros([size(x,1),2,2]);
        rm2 = (x.^2 + y.^2).^(-1);
        rr(:,1,1) = x.*x.*rm2;
        rr(:,1,2) = x.*y.*rm2;
        rr(:,2,1) = y.*x.*rm2;
        rr(:,2,2) = y.*y.*rm2;
    end

function rsInv = rsInverse(x,y)
        rsInv = ((x.^2 + y.^2).^(1/2) - (r1+r2))/r1;
        rsInv = rsInv.^(-1);
        rsInv = rsInv(:,ones(4,1));
        rsInv = reshape(rsInv,[],2,2);
end

    function id = IoxI(N)
        id = zeros(N,2,2);
        id(:,1,1) = 1; id(:,2,2) = 1;
    end


end