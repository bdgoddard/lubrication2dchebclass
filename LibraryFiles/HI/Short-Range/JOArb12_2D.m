function HI = JOArb12_2D(x,y,optsPhys)

N = length(x);

rr = roxr(x,y);
id = IoxI(N);

% Get needed params
% sigmaH = optsPhys.sigmaHS(:);
% StokesEinsteinNumberH = optsPhys.StokesEinsteinNumberS(:);

beta = optsPhys.beta;

r1 = 1/2;
r2 = beta/2;

% r1 = 1; r2 = beta;

dPts = (x.^2+y.^2).^(1/2);
% convpts = (x.^2+y.^2).^(1/2);
% convptsforsurfaces = convpts-(r1+r2);

% Get pointwise force
% toleta = 1e-9;
normalInteraction = zeros(1,N);
tangentialInteraction = zeros(1,N);

if beta ==1
    disp('Computing common species forces...');
else
    disp('Computing cross species forces...');
end

for i=1:N
    if (~isinf(dPts(i)))
        normalInteraction(i) = -JOArbXA11(beta,dPts(i));
        tangentialInteraction(i) = -JOArbYA11(beta,dPts(i));
    else
        normalInteraction(i) = -1;
        tangentialInteraction(i) = -1;
    end
end


Ftemp1 = normalInteraction(1,:)';
% Ftemp1(Ftemp1==0) = -1;
F1 = Ftemp1 + 1;

Ftemp2 = tangentialInteraction(1,:)';
% Ftemp2(Ftemp2==0) = -1;
F2 = Ftemp2 + 1;


F1 = F1(:,ones(4,1));
F1 = reshape(F1,[],2,2);
F2 = F2(:,ones(4,1));
F2 = reshape(F2,[],2,2);

HI = F1.*rr + F2.*(id-rr);

HI(isnan(HI))=0;

    function rr = roxr(x,y)
        % x and y are the kron products
        rr = zeros([size(x,1),2,2]);
        rm2 = (x.^2 + y.^2).^(-1);
        rr(:,1,1) = x.*x.*rm2;
        rr(:,1,2) = x.*y.*rm2;
        rr(:,2,1) = y.*x.*rm2;
        rr(:,2,2) = y.*y.*rm2;
    end

    function id = IoxI(N)
        id = zeros(N,2,2);
        id(:,1,1) = 1; id(:,2,2) = 1;
    end



end