%% Jeffrey-Onishi 1984
% FUNCTION XA11 TO COMPUTE SCALAR FUNCTION XA11 (3.20) using 15 f_m's
function X = JOArbXA11(lambda,d)

r1 = 1/2; r2 = lambda/2;

s = 2*d/(r1+r2);

l = lambda;

f = fMatrix();
lP = lambdaPowers(l);
onePluslP = onePlusLambdaPowers(l);

inversePowersOfTwo = 2.^([0:-1:-15]');
powersOfTwoOverS = (2./s).^([0:15]');

s1 = (f*lP).*onePluslP;
s1 = s1.*inversePowersOfTwo;
s1 = s1.*powersOfTwoOverS;

s2 = -2*g2(l)*[0; 1./([1:15]')];
s2 = s2.*powersOfTwoOverS;

s3 = 4*g3(l)*[0; 1./([1:15]')].*((m1([0:1:15]')).^(-1));
s3 = s3.*powersOfTwoOverS;

s4 = -g1(l)*ones(16,1);
s4 = s4.*powersOfTwoOverS;
% s2 = -2*1^([1:-1:-11]')*g2(l);
% s3 = 4*g3(l)*1^([1:-1:-11]').*((m1([1:1:11]')).^(-1));
S = 0;
S = S + s1 + s2 + s3 + s4;

S = S(3:2:end); % sum over even integers beginning at 2

X = g1(l)*(1-4*s.^(-2)).^(-1) -g2(l)*log(1-4*s.^(-2))...
    -g3(l)*(1-4*s.^(-2)).*log(1-4*s.^(-2)) + 1 -g1(l);
X = X + sum(S);

% fEvenOnly = f(1:2:end,:);
% lPEvenOnly = lP(1:2:end);
% onePluslPEvenOnly = onePluslP(1:2:end);
% evenPowersOfTwo = 2.^([0:-2:-10]');

%
%
% AX11TEMP = f*AX11SUMMANDl(lambda); ax11tempeval = AX11TEMP(3:2:end);
%
% ax11sqeval = (AX11SUMMANDQUOTIENT(lambda)); ax11sqeval = ax11sqeval(2:end);
%
% ax11tempeval = ax11tempeval.*ax11sqeval;
%
% s1 = 2.^(-(2:2:14))*ax11tempeval -7*1/4;
%
% s1 = s1 - 2*((2:2:14).^(-1))*ones(7,1)*g2(lambda);
%
% s1 = s1 + 4*((2:2:14).^(-1))*(m1(2:2:14).^(-1)')*g3(lambda);
%
% A = 1 - 1/4*g1(lambda) + s1;
%
    function z = m1(m)
        z = -2*eq(m,2) + (m-2).*(1-eq(m,2));
    end

    function z = g1(l)
        z = 2*l^2./(1+l).^3;
    end

    function z = g2(l)
        z = 1/5*l.*(1 + 7*l + l.^2)./(1+l).^3;
    end

    function z = g3(l)
        z =  1/42*(1 +18.*l-29.*l.^2 +18.*l.^3+16.*l.^4)./(1+l)^3;
    end

    function f = fMatrix()
        f(1,1) = 1; f(2,2) = 3; f(3,2) = 9; f(4,2:4) = [-4,27,-4];
        f(5,2:4) = [-24,81,36]; f(6,3:5) = [72,243,72]; f(7,2:6) = [16,108,281,648,144];
        f(8,3:7) = [288,1620,1515,1620,288];
        f(9,3:8) = [576,4848,5409,4524,3888,576];
        f(10,3:9) = [1152,9072,14752,26163,14752,9072,1152];
        f(11,3:10) = [2304,20736,42804,115849,76176,39264,20736,2304];
        f(12,3:11) = [4608,46656,108912,269100,319899,269100,108912,46656,4608];
        f(13,3:12) = [9216,103680,286160,847800,1007697,1551540,727200,272000,103680,9216];
        f(14,3:13) = [18432,228096,695808,2208672,4094456,4434723,4094456,2208672,695808,228096,18432];
        f(15,3:14) = [36864,497664,1725504,6296112,11277948,17399673,14649048,15577200,5679360,1668864,497664,36864];
        f(16,3:15) = [73728,1078272,4066048,15788160,39777920,53394756,74752459,53394756,39777920,15788160,4066048,1078272,73728];
    end

    function lP = lambdaPowers(l)
        lP = [1; l; l.^2; l.^3; l.^4; l.^5; l.^6; l.^7; l.^8; l.^9; l.^10;....
                l.^11; l.^12; l.^13; l.^14];
    end

    function onePluslP = onePlusLambdaPowers(l)
        onePluslP = [1; 1./(1+l); 1./(1+l).^2 ;1./(1+l).^3 ;1./(1+l).^4;...
            1./(1+l).^5; 1./(1+l).^6; 1./(1+l).^7; 1./(1+l).^8;...
            1./(1+l).^9; 1./(1+l).^10; 1./(1+l).^11; 1./(1+l).^12; ...
            1./(1+l).^13; 1./(1+l).^14; 1./(1+l).^15];
    end



end