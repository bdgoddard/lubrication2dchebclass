function [FSphere1, FSphere2] = makeNormalScalar(centredistance,sigmaH,tol)
% SUMFORCE gives the nondimensional hydrodynamic force on sphere1/sphere2
% from sphere2/sphere1 given surface distance and radii.

d = centredistance;

r2 = sigmaH/2;
r1 = 1/2;

% r2 = beta/2;
% r1 = 1/2;

% Get etas

% eta1eta2 = Eta1Eta2(r1,r2,d,toleta);
eta1eta2 = getEtas(r1,r2,d);
eta1eval = eta1eta2(1);
eta2eval = eta1eta2(2);

% Get forces
FSphere1 = normalForceSphere1(eta1eval,eta2eval);
FSphere2 = normalForceSphere2(eta1eval,eta2eval);

% sphere1
    function f1 = normalForceSphere1(eta1,eta2)
        if isnan(eta1)
            f1 = -1;
        else
            result = 0;
            err1 = abs(result-1);
            resultold = result;
            n=1;
            while err1>tol
                result = resultold + sinh(eta1)*sqrt(2)/6*(2*n+1)*(an(n,eta1,eta2)+bn(n,eta1,eta2)+cn(n,eta1,eta2)+dn(n,eta1,eta2));
                err1 = norm(result-resultold,2);
                resultold=result;
                n=n+1;
            end
        f1=result;
        end
    end
FSphere1(FSphere1 == 0) = 1*sign(sinh(eta1eval));
% sphere 2

    function f2 = normalForceSphere2(eta1,eta2)
        if isnan(eta2)
            f2 = 1;
        else
            result = 0;
            err2 = abs(result-1);
            resultold = result;
            n=1;
            while err2>tol
                result = resultold + sinh(eta2)*sqrt(2)/6*(2*n+1)*(-an(n,eta1,eta2)+bn(n,eta1,eta2)-cn(n,eta1,eta2)+dn(n,eta1,eta2));
                err2 = norm(result-resultold,2);
                resultold=result;
                n=n+1;
            end
        f2=result;
        end
    end
FSphere2(FSphere2 == 0) = 1*sign(sinh(eta2eval));

    function c = prefactor(n)
        c = sqrt(2)*n*(n+1)/(2*n+1)/(2*n-1)/(2*n+3);
    end

    function a = an(n,eta1,eta2)
        a = (-(((3+2*n)*prefactor(n)*((-1+4*n^2)*cosh(2*eta1) + (1-4*n^2)*cosh(2*eta2)...
            +2*((1-2*n)*cosh(eta1+2*eta1*n) + (1+2*n)*cosh(eta1-2*eta2 + 2*eta1*n) - cosh(2*eta1 - eta2 - 2*eta2*n)...
            -2*n*cosh(2*eta1 - eta2 - 2*eta2*n) - cosh(eta2 + 2*eta2*n)+2*n*cosh(eta2 + 2*eta2*n))))...
            /(3-4*n -4*n^2 + (1 + 2*n)^2*cosh(2*(eta1 - eta2)) - 4*cosh((eta1 - eta2)*(1 + 2*n)))));
    end

    function b = bn(n,eta1,eta2)
        b = (((3 + 2*n)*prefactor(n)*(3 - 4*n - 4*n^2 + (1 + 2*n)^2*cosh(2*(eta1 - eta2))...
            -4*cosh((eta1 - eta2)*(1 + 2*n)) + sinh(2*eta1) - 4*n^2*sinh(2*eta1) + sinh(2*(eta1 - eta2))...
            + 4*n*sinh(2*(eta1 - eta2)) + 4*n^2*sinh(2*(eta1 - eta2)) -sinh(2*eta2) + 4*n^2*sinh(2*eta2)...
            + 4*sinh((eta1 - eta2)*(1 + 2*n)) + 2*sinh(eta1 + 2*eta1*n) - 4*n*sinh(eta1 + 2*eta1*n)...
            + 2*sinh(eta1 - 2*eta2 + 2*eta1*n) + 4*n*sinh(eta1 - 2*eta2 + 2*eta1*n) + 2*sinh(2*eta1 - eta2 - 2*eta2*n)...
            + 4*n*sinh(2*eta1 - eta2 - 2*eta2*n) - 2*sinh(eta2 + 2*eta2*n) + 4*n*sinh(eta2 + 2*eta2*n)))...
            /(3 - 4*n - 4*n^2 + (1 + 2*n)^2*cosh(2*(eta1 - eta2)) - 4*cosh((eta1 - eta2)*(1 + 2*n))));
    end

    function c = cn(n,eta1,eta2)
        c = (((-1 + 2*n)*prefactor(n)*((3 + 8*n + 4*n^2)*cosh(2*eta1) - (3 + 8*n + 4*n^2)*cosh(2*eta2)...
            +2*((3 + 2*n)*cosh(eta1 + 2*eta1*n) - (1 + 2*n)*cosh(eta1 + 2*eta2 + 2*eta1*n) - 3*cosh(eta2 + 2*eta2*n)...
            -2*n*cosh(eta2 + 2*eta2*n) + cosh(2*eta1 + eta2 + 2*eta2*n) +2*n*cosh(2*eta1 + eta2 + 2*eta2*n))))...
            /(3 - 4*n -4*n^2 + (1 + 2*n)^2*cosh(2*(eta1 - eta2)) -4*cosh((eta1 - eta2)*(1 + 2*n))));
    end

    function d = dn(n,eta1,eta2)
        d = (-(((-1 + 2*n)*prefactor(n)*(3 - 4*n - 4*n^2 + (1 + 2*n)^2*cosh(2*(eta1 - eta2))...
            -4*cosh((eta1 - eta2)*(1 + 2*n)) + 3*sinh(2*eta1) + 8*n*sinh(2*eta1)...
            + 4*n^2*sinh(2*eta1) - sinh(2*(eta1 - eta2)) - 4*n*sinh(2*(eta1 - eta2)) - 4*n^2*sinh(2*(eta1 - eta2))...
            - 3*sinh(2*eta2) - 8*n*sinh(2*eta2) - 4*n^2*sinh(2*eta2) + 4*sinh((eta1 - eta2)*(1 + 2*n))...
            + 6*sinh(eta1 + 2*eta1*n) + 4*n*sinh(eta1 + 2*eta1*n) - 2*sinh(eta1 + 2*eta2 + 2*eta1*n)...
            - 4*n*sinh(eta1 + 2*eta2 + 2*eta1*n) - 6*sinh(eta2 + 2*eta2*n) - 4*n*sinh(eta2 + 2*eta2*n)...
            + 2*sinh(2*eta1 + eta2 + 2*eta2*n) + 4*n*sinh(2*eta1 + eta2 + 2*eta2*n)))...
            /(3 - 4*n - 4*n^2 + (1 + 2*n)^2*cosh(2*(eta1 - eta2)) - 4*cosh((eta1 - eta2)*(1 + 2*n)))));
    end

end
