function eta1eta2 = getEtas(r1,r2,d)
% ETA1ETA2 calculates the eta1 and eta2 ordinates by Newton interation
% WE TAKE CARE WITH THE FACT THAT \ETA2<0! IN DEFINITION OF THE ROOT
e0 = [1,-2]';

% options = optimoptions('fsolve','Display','off', 'FunctionTolerance', 1e-12);
options = optimoptions(@fsolve,'Display','off',...
    'Algorithm','trust-region',...
    'SpecifyObjectiveGradient',true,'OptimalityTolerance', 1e-16);


if d == inf
    eta1eta2 = [nan;nan];
else
    eta1eta2 = fsolve(@geomFun,e0,options);
end

    function [E, J] = geomFun(e1e2)
        eta1 = e1e2(1);
        eta2 = e1e2(2);
        
        E(1) = r1*sinh(eta1) + r2*sinh(eta2);
        E(2) = r1*cosh(eta1) + r2*cosh(eta2) -d;
        
        if nargout > 1
            J = [r1*cosh(eta1), r2*cosh(eta2); r1*sinh(eta1), r2*sinh(eta2)];
        end
        
    end
end
