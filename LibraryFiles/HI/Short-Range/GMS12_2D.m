function HI = GMS12_2D(x,y,optsPhys)
N = length(x);

rr = roxr(x,y);
id = IoxI(N);

% Get needed params
beta = optsPhys.beta;
r1 = 1/2;
r2 = beta/2;

convpts = (x.^2+y.^2).^(1/2);

% Get pointwise force
toleta = 1e-9;
normalForce = zeros(2,N);
tangentForce = zeros(size(normalForce));

if beta == 1
    disp('Computing common species forces...');
else
    disp('Computing cross species forces...');
end

for i=1:N
    if (~isinf(convpts(i)))
        [normalForce(1,i), ~] = makeNormalScalar(convpts(i),beta,toleta);
        [tangentForce(1,i), ~] = makeTangentScalar(convpts(i),beta,toleta);
    else
        normalForce(1,i) = -1;
        tangentForce(1,i) = -1;
    end
end


nForce = normalForce(1,:)';
tForce = tangentForce(1,:)';

nForce(nForce==0) = -1;
nForceNoDrag = nForce + 1;
nForceNoDrag = nForceNoDrag(:,ones(4,1));
nForceNoDrag = reshape(nForceNoDrag,[],2,2);

stokesindex = min(find(tForce==-1));
tForce(stokesindex:end)=-1;
tForce(isnan(tForce))=-1;
tForceNoDrag = tForce + 1;


tForceNoDrag = tForceNoDrag(:,ones(4,1));
tForceNoDrag = reshape(tForceNoDrag,[],2,2);
HI = nForceNoDrag.*rr + tForceNoDrag.*(id-rr);

HI(isnan(HI))=0;


    function rr = roxr(x,y)
        % x and y are the kron products
        rr = zeros([size(x,1),2,2]);
        rm2 = (x.^2 + y.^2).^(-1);
        rr(:,1,1) = x.*x.*rm2;
        rr(:,1,2) = x.*y.*rm2;
        rr(:,2,1) = y.*x.*rm2;
        rr(:,2,2) = y.*y.*rm2;
    end

    function id = IoxI(N)
        id = zeros(N,2,2);
        id(:,1,1) = 1; id(:,2,2) = 1;
    end


end