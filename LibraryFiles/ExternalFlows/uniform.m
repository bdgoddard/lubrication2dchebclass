function [uvExt] = uniform(x,y,opts)
    uExt = ones(size(x));
    vExt = zeros(size(y));

    uvExt = [uExt;vExt];

end