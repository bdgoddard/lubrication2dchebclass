function [uvExt] = testFlow(x,y)
%     x = IDC.Pts.x;
%     y = IDC.Pts.y;
%     
    uExt = 1e-1*ones(size(x));
    vExt = zeros(size(y));

    uvExt = [uExt;vExt];

end