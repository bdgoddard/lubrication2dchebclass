function [uvExt] = poiseuilleFlow(x,y)
%     x = IDC.Pts.x;
%     y = IDC.Pts.y;
%     
    cutoff = (erf(x+10)-erf(x-10))/2;
    uExt = 1e-1*(y+5).*(10-(y+5)).*cutoff;
    vExt = zeros(size(y));
  
    uvExt = [uExt;vExt];
    
    

end