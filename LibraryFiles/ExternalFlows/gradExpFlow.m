function [uvExt] = poiseuilleFlow(x,y)
%     x = IDC.Pts.x;
%     y = IDC.Pts.y;
%     

    uExt = 1e-1*(y+5).*(10-(y+5));
    vExt = zeros(size(y));

    uvExt = [uExt;vExt];

end