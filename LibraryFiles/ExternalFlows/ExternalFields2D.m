function ExternalFlowStruct = ExternalFields2D(opts,IDC)

    optsPhys = opts.optsPhys;    
    optsNum  = opts.optsNum;

    if(isfield(optsPhys,'nParticlesS'))
        nSpecies=length(optsPhys.nParticlesS);
    else
        nSpecies=optsPhys.nSpecies;
    end

%     params = optsPhys.ExternalFlow;
    
    optsNum = optsNum.ExternalFlowNum;
    
%     if(isfield(optsNum,'HIPreprocess'))
%         fPreprocess = str2func(optsNum.HIPreprocess);
%         params = fPreprocess(params);
%     end
   
    f1      = str2func(optsNum.externalFlow);
%     f12      = str2func(optsNum.HI12);
    
    if(isfield(optsNum,'N'))
        params.N = optsNum.N;
    else
        params.N = IDC.N;
    end
    
    if(isfield(optsNum,'L'))
        params.L = optsNum.L;
    elseif(isfield(IDC,'L'))
        params.L = IDC.L;
    end
    
    xpts = IDC.Pts.x;
    ypts = IDC.Pts.y;
    
    paramNames = fieldnames(params);
    nParams = length(paramNames);
    
    ExternalFlowStruct(nSpecies,nSpecies).externalAdvective = [];
    
    for iS = 1:nSpecies
        for jS = iS:nSpecies
%             paramsIJ = getIJParams(iS,jS);

            
%             HITemp11 = IDC.ComputeConvolutionMatrix(@flowFunction,paramsIJ);  
%             HITemp12 = IDC.ComputeConvolutionMatrix(@F12,paramsIJ); 
%             
%             HIInt11 = [HITemp11(:,:,1,1), HITemp11(:,:,1,2) ; ...
%                        HITemp11(:,:,2,1), HITemp11(:,:,2,2) ];
% 
%             HIInt12 = [HITemp12(:,:,1,1), HITemp12(:,:,1,2) ; ...
%                        HITemp12(:,:,2,1), HITemp12(:,:,2,2) ];                   
                   
            ExternalFlowStruct(iS,jS).externalField = flowFunction(xpts,ypts);
            
        end
    end

    %--------------------------------------------------------------------------
    function z = flowFunction(x,y)         
        z = f1(x,y);                    
    end

%     function z = F12(x,y)         
%         z = f12(x,y,paramsIJ);                    
%     end

%     function paramsIJ = getIJParams(iS,jS)
% 
%        paramsIJ = params;
% 
%         for iParam=1:nParams
%             paramValues = params.(paramNames{iParam});
%             
%             if(size(paramValues,1)==nSpecies && size(paramValues,2)==nSpecies)
%                 paramIJ = paramValues(iS,jS,:);
%                 paramsIJ.(paramNames{iParam}) = paramIJ;
%             end
% 
%         end           
%     end

end