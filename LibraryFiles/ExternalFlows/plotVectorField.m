X = 8; Y = 5;

geom.N = [20,20]; geom.L1 = 4; geom.y2Min = -5; geom.y2Max = 5;
geom.N2bound = 10;
geom.R = 1;

x1Plot = linspace(-0.9,0.9,100)';
x2Plot = linspace(-1,1,100)';


acap = InfCapillary_FMT(geom);
acap.ComputeAll;
acap.ComputeInterpolationMatrix(x1Plot,x2Plot,true,true);

yy = acap.Pts.y2_kv;
xx = acap.Pts.y1_kv;

% [x,y] = meshgrid(-X:0.2:X,-Y:0.2:Y);

% u = .1*sin(y);
% v = zeros(size(x));

% v = .1*sin(y);
%  v = zeros(size(y));


[uv] = poiseuilleFlow(xx,yy);
% u = (yy+5).*(10-(yy+5));
% v = zeros(size(xx));

acap.plotFlux(uv);


