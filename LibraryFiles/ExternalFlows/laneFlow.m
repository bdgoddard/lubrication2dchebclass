function [uvExt] = laneFlow(x,y)
%     x = IDC.Pts.x;
%     y = IDC.Pts.y;
%     

    uExt = 1e-1*sin(y);
    vExt = zeros(size(y));

    uvExt = [uExt;vExt];

end