%% GETPERTURBATIONFUNCTIONS 
% RETRIEVES THE PHYSICIAL EIGENVECTORS AND CORRESPONDING EIGENVALUES OF THE CONV (OR INT KERNEL)
% OPERATOR USED TO BIFURCATE STABLE STATIONARY DENSITIES

function [efuns, evals] = getPerturbationFuns(R)
    R(isnan(R)) = 0; R(isinf(R)) = 0;
    % Compute eigenfunctions and eigenvectors
    [efuns,evals] = eig(R);
    
%     uniform = 1/4*ones(size(Pts.y));
%     % R is mean zero
%     max(abs(R*uniform))
    
    evals = diag(evals);
    
    % Remove nonphysicial ones
    physIndex = abs(evals)>1e-15;
    efuns = efuns(:,physIndex);
    evals = evals(physIndex);

end