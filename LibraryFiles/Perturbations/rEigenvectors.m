function [ws,betas] = rEigenvectors(rOp,bcMat,paramsEig)

% k2 = paramsEig.k2
rhok2 = paramsEig.rhok2;

B = diag(rhok2.^(-1))*bcMat;
% B = bcMat;

% rhorep = repmat(rhok2,1,40);
% rOp = rhorep.*rOp;
[wvectors,bvalues] = eig(rOp,B);
% [wvectors,bvalues] = eig(rOp);


ws = wvectors;
betas = diag(bvalues);

% bifIndex = find(betans==-k2);
% 
% if isempty(bifIndex)
%     w1 = [];
% else
%     w1 = wvectors(:,bifIndex);
% end

end