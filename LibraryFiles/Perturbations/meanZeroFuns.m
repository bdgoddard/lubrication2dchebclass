function rhoPerturb = meanZeroFuns(y,opts, optsNum)

alpha = opts.alpha;
rho = opts.rho;
convolutionMat = optsNum.convolutionOp;
interactionEnergy = opt.kappa2;
numPts = optsNum.N;

rhos = repmat(rho,1,N);



M = convolutionMat

L1 = min(y)/2;
L2 = max(y)/2;

mask = ( tanh((y-L1)/.1)-tanh((y-L2)/.1) )/2;
% mask = 1;

perturb = sin(2*pi*y);
% perturb = y.^3;

rhoPerturb = alpha*mask.*perturb;

end