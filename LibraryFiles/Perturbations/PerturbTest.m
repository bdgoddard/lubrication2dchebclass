function rhoPerturb = PerturbTest(y,opts)

alpha = opts.alpha;
L1 = min(y)/2;
L2 = max(y)/2;

mask = (tanh((y-L1)/.1)-tanh((y-L2)/.1))/2;
% mask = 1;

% perturb = cos(4*pi*y);
perturb = sin(2*pi*y/L1);
% perturb = y.^3;

rhoPerturb = alpha*mask.*perturb;

end